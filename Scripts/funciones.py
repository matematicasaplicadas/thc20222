# -*- coding: utf-8 -*-
"""
Created on Tue Mar 29 13:41:46 2022

@author: Ruíz Martínez Gustavo Rafael
"""

# %% Catetos e hipotenusa de un triángulo rectángulo v6.0 29 03 2022

"""
Ejercicio individual

Problema
Dado un triángulo rectángulo de lados a, b, c unidades 
respectivamente, calcular el lado faltante.

Solución:

Identificar de los datos de entrada (a, b, c) cuál de ellos tiene el 
valor 0 y calcularlo haciendo uso de la fórmula correspondiente.

Si c tiene el valor 0 y a y b son mayores de 0, entonces utilizamos la fórmula:
   c = √(a**2 + b**2)
   
Si a tiene el valor 0 y b y c son mayores de 0, entonces utilizamos la fórmula: 
    a = √(c**2 - b**2)
    
Si b tiene el valor 0 y a y c son mayores de 0, entonces utilizamos la fórmula:
    b = √(c**2 - a**2) 

Donde a**2 significa a elevado al cuadrado, b**2 significa b 
elevado al cuadrado y c**2 significa c elevado al cuadrado; además,
a y b son catetos y c la hipotenusa del triángulo rectángulo.

El problema solo tiene sentido cuando los tres lados del triángulo tienen
longitudes positivas.
"""




import math
def clasificación_triángulo_longitud(a, b, c):
    if c == 0 and a > 0 and b > 0:  # and sirve para meter restricciones adicionales
        c = math.sqrt(a**2 + b**2)
        mensaje = "El triángulo de cateto a = {} y cateto b = {}, " \
            "tiene hipotenusa c = {:0.4}".format(a, b, c)

    elif a == 0 and b > 0 and c > 0:     # No puede haber un elif sin un if
        a = math.sqrt(c**2 - b**2)
        mensaje = "El triángulo de cateto b = {} e hipotenusa c = {}, " \
            "tiene cateto a = {:0.4}".format(b, c, a)

    elif b ==0 and a>0 and c>0:  # else no lleva condicionales y no puede haber un else sin un if
        b = math.sqrt(c**2 - a**2)
        mensaje = "El triángulo de cateto a = {} e hipotenusa c = {}, " \
            "tiene cateto b = {:0.4}".format(a, c, b)

    else:
        mensaje = "Solo uno de los valores puede ser cero" \
            " y los otros dos tienen que ser positivos."
        
    return mensaje  # si agrego al código print, es porque quiero que
    # no muestre el resultado, si no, solo lo procesa y no lo
    # guarda


print("Escribe 0 en caso de no conocer la longitud\n")
a = float(input("Escribe la longitud del cateto a: "))
b = float(input("Escribe la longitud del cateto b: "))
c = float(input("Escribe la longitud de la hipotenusa c: "))

mensaje = clasificación_triángulo_longitud(a, b, c)
print(mensaje)

# %%

print(clasificación_triángulo_longitud(3, 4, 0))
print(clasificación_triángulo_longitud(0, 4, 5))
print(clasificación_triángulo_longitud(3, 0, 5))
print(clasificación_triángulo_longitud(3, 0, 0))

# %% Tricotomía 29 03 2022

def tricotomía(a):
    if a ==0:
        print("Es cero")
    elif a > 0:            #Puede haber tantos elif como sean necesarios
        print("Es positivo")
    else:
        print("Es negativo")

tricotomía(-8)
tricotomía(81)
tricotomía(56)
tricotomía(-1024)
tricotomía(0)

# %% Tricotomía v2.0 29 03 2022

def tricotomía(a):
    if a ==0:
       mensaje = "Es cero"
    elif a > 0:            #Puede haber tantos elif como sean necesarios
        mensaje = "Es positivo"
    else:
        mensaje = "Es negativo"
    return mensaje

resultado1 = tricotomía(-8)
resultado2 = tricotomía(81)
resultado3 = tricotomía(56)
print(resultado1)
print(resultado2)
print(resultado3)
print(tricotomía(-1024))
print(tricotomía(0))

"""
valor_absoluto
"""

# %% Verificar si un triángulo es rectángulo 29 03 2022
""" 
Problema
Identificar si un triángulo de lados a,b c es rectángulo 

Solución
Si c^2 = b^2 + c^2, entonces el triángulo de lados a, b, c es rectángulo.
Si c^2 <> b^2 + c^2, entonces el triángulo de lados a, b, c, no es 
rectángulo.
"""

a = float(input("Escribe la longitud del cateto a: "))
b = float(input("Escribe la longitud del cateto b: "))
c = float(input("Escribe la longitud de la hipotenusa c: "))

if c**2 == a**2 + b**2:
    mensaje = "Es un triángulo rectángulo"
else: 
    mensaje = "No es un triángulo rectángulo"
    
print(mensaje)

# %% Verificar si un triángulo es rectángulo v2.0 29 03 2022
""" 
Problema
Identificar si un triángulo de lados a,b c es rectángulo 

Solución
Si c^2 = b^2 + c^2, entonces el triángulo de lados a, b, c es rectángulo.
Si c^2 <> b^2 + c^2, entonces el triángulo de lados a, b, c, no es 
rectángulo.
"""

def triángulo_rectángulo(a,b,c):
    if c**2 == a**2 + b**2:
        mensaje = "Es un triángulo rectángulo"
    else: 
        mensaje = "No es un triángulo rectángulo"
    return mensaje

a = float(input("Escribe la longitud del cateto a: "))
b = float(input("Escribe la longitud del cateto b: "))
c = float(input("Escribe la longitud de la hipotenusa c: "))

mensaje = triángulo_rectángulo(a,b,c)
print(mensaje)