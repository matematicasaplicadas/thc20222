# -*- coding: utf-8 -*-
"""
Created on Tue Mar 15 14:29:26 2022

@author: Ruíz Martínez Gustavo Rafael
"""

# %% Hipotenusa de un triángulo rectángulo 10 03 2022

"""
Ejercicio individual

Problema
Dado un triángulo rectángulo de lados a, b, c unidades 
respectivamente, calcular el lado faltante.

Solución:

Identificar de los datos de entrada (a, b, c) cuál de ellos tiene el 
valor 0 y calcularlo haciendo uso de la fórmula correspondiente.

Si c tiene el valor 0, entonces utilizamos la fórmula:
   c = √(a**2 + b**2)
   
Si a tiene el valor 0, entonces utilizamos la fórmula: 
    a = √(c**2 - b**2)
    
Si b tiene el valor 0, entonces utilizamos la fórmula:
    b = √(c**2 - a**2) 

Donde a**2 significa a elevado al cuadrado, b**2 significa b 
elevado al cuadrado y c**2 significa c elevado al cuadrado; además,
a y b son catetos y c la hipotenusa del triángulo rectángulo.

"""
import math

print("Escribe 0 en caso de no conocer la longitud\n")
a = float(input("Escribe la longitud del cateto a: "))
b = float(input("Escribe la longitud del cateto b: "))
c = float(input("Escribe la longitud de la hipotenusa c: "))

if c==0: #un = es asignación, dos == son comparación
    c = math.sqrt(a**2 + b**2)    # verificar si un determinado valor
#a = math.sqrt(c**2 - b**2)       # está almacenado en una variable 
#b = math.sqrt(c**2 - a**2)       # o dos variables tienen un mismo valor
#c = math.sqrt(a**2 + b**2)

print("El triángulo de cateto a = {} e hipotenusa c = {}, "
      "tiene cateto b = {:0.4}".format(a, c, b))


# %% Catetos e hipotenusa de un triángulo rectángulo 15 03 2022

"""
Ejercicio individual

Problema
Dado un triángulo rectángulo de lados a, b, c unidades 
respectivamente, calcular el lado faltante.

Solución:

Identificar de los datos de entrada (a, b, c) cuál de ellos tiene el 
valor 0 y calcularlo haciendo uso de la fórmula correspondiente.

Si c tiene el valor 0, entonces utilizamos la fórmula:
   c = √(a**2 + b**2)
   
Si a tiene el valor 0, entonces utilizamos la fórmula: 
    a = √(c**2 - b**2)
    
Si b tiene el valor 0, entonces utilizamos la fórmula:
    b = √(c**2 - a**2) 

Donde a**2 significa a elevado al cuadrado, b**2 significa b 
elevado al cuadrado y c**2 significa c elevado al cuadrado; además,
a y b son catetos y c la hipotenusa del triángulo rectángulo.

"""
import math

print("Escribe 0 en caso de no conocer la longitud\n")
a = float(input("Escribe la longitud del cateto a: "))
b = float(input("Escribe la longitud del cateto b: "))
c = float(input("Escribe la longitud de la hipotenusa c: "))

if c==0: #un = es asignación, dos == son comparación
    c = math.sqrt(a**2 + b**2)    # verificar si un determinado valor
                                  # está almacenado en una variable 
                                  # o dos variables tienen un mismo valor

if a==0:
    a = math.sqrt(c**2 - b**2)   
    
if b==0:
    b = math.sqrt(c**2 - a**2)
    
#a = math.sqrt(c**2 - b**2)
#b = math.sqrt(c**2 - a**2)
#c = math.sqrt(a**2 + b**2)

print("El triángulo de cateto a = {} e hipotenusa c = {}, "
      "tiene cateto b = {:0.4}".format(a, c, b))

# %% Catetos e hipotenusa de un triángulo rectángulo v2.0 15 03 2022

"""
Ejercicio individual

Problema
Dado un triángulo rectángulo de lados a, b, c unidades 
respectivamente, calcular el lado faltante.

Solución:

Identificar de los datos de entrada (a, b, c) cuál de ellos tiene el 
valor 0 y calcularlo haciendo uso de la fórmula correspondiente.

Si c tiene el valor 0 y a y b son mayores de 0, entonces utilizamos la fórmula:
   c = √(a**2 + b**2)
   
Si a tiene el valor 0 y b y c son mayores de 0, entonces utilizamos la fórmula: 
    a = √(c**2 - b**2)
    
Si b tiene el valor 0 y a y c son mayores de 0, entonces utilizamos la fórmula:
    b = √(c**2 - a**2) 

Donde a**2 significa a elevado al cuadrado, b**2 significa b 
elevado al cuadrado y c**2 significa c elevado al cuadrado; además,
a y b son catetos y c la hipotenusa del triángulo rectángulo.

El problema solo tiene sentido cuando los tres lados del triángulo tienen
longitudes positivas.
"""
import math

print("Escribe 0 en caso de no conocer la longitud\n")
a = float(input("Escribe la longitud del cateto a: "))
b = float(input("Escribe la longitud del cateto b: "))
c = float(input("Escribe la longitud de la hipotenusa c: "))

if c==0 and a>0 and b>0:     #and sirve para meter restricciones adicionales
    c = math.sqrt(a**2 + b**2)    
                                 
if a==0 and b>0 and c>0:     # Un and va después de una comparación
    a = math.sqrt(c**2 - b**2)   

if b==0 and a>0 and c>0:
    b = math.sqrt(c**2 - a**2)
    
#a = math.sqrt(c**2 - b**2)
#b = math.sqrt(c**2 - a**2)
#c = math.sqrt(a**2 + b**2)


if a>0 and b>0 and c>0:
    print("El triángulo de cateto a = {} e hipotenusa c = {}, "
      "tiene cateto b = {:0.4}".format(a, c, b))
    
# %% Catetos e hipotenusa de un triángulo rectángulo v3.0 15 03 2022

"""
Ejercicio individual

Problema
Dado un triángulo rectángulo de lados a, b, c unidades 
respectivamente, calcular el lado faltante.

Solución:

Identificar de los datos de entrada (a, b, c) cuál de ellos tiene el 
valor 0 y calcularlo haciendo uso de la fórmula correspondiente.

Si c tiene el valor 0 y a y b son mayores de 0, entonces utilizamos la fórmula:
   c = √(a**2 + b**2)
   
Si a tiene el valor 0 y b y c son mayores de 0, entonces utilizamos la fórmula: 
    a = √(c**2 - b**2)
    
Si b tiene el valor 0 y a y c son mayores de 0, entonces utilizamos la fórmula:
    b = √(c**2 - a**2) 

Donde a**2 significa a elevado al cuadrado, b**2 significa b 
elevado al cuadrado y c**2 significa c elevado al cuadrado; además,
a y b son catetos y c la hipotenusa del triángulo rectángulo.

El problema solo tiene sentido cuando los tres lados del triángulo tienen
longitudes positivas.
"""
import math

print("Escribe 0 en caso de no conocer la longitud\n")
a = float(input("Escribe la longitud del cateto a: "))
b = float(input("Escribe la longitud del cateto b: "))
c = float(input("Escribe la longitud de la hipotenusa c: "))

#a = math.sqrt(c**2 - b**2)
#b = math.sqrt(c**2 - a**2)
#c = math.sqrt(a**2 + b**2)

if c==0 and a>0 and b>0:     #and sirve para meter restricciones adicionales
    c = math.sqrt(a**2 + b**2)    
    mensaje = "El triángulo de cateto a = {} y cateto b = {}, " \
    "tiene hipotenusa c = {:0.4}".format(a, b, c)
      
if a==0 and b>0 and c>0:     # Un and va después de una comparación
    a = math.sqrt(c**2 - b**2)   
    mensaje = "El triángulo de cateto b = {} e hipotenusa c = {}, " \
    "tiene cateto a = {:0.4}".format(b, c, a)

if b==0 and a>0 and c>0:
    b = math.sqrt(c**2 - a**2)
    mensaje = "El triángulo de cateto a = {} e hipotenusa c = {}, " \
    "tiene cateto b = {:0.4}".format(a, c, b)
    
if a>0 and b>0 and c>0:
    print(mensaje)
    

# %% Catetos e hipotenusa de un triángulo rectángulo v4.0 17 03 2022

"""
Ejercicio individual

Problema
Dado un triángulo rectángulo de lados a, b, c unidades 
respectivamente, calcular el lado faltante.

Solución:

Identificar de los datos de entrada (a, b, c) cuál de ellos tiene el 
valor 0 y calcularlo haciendo uso de la fórmula correspondiente.

Si c tiene el valor 0 y a y b son mayores de 0, entonces utilizamos la fórmula:
   c = √(a**2 + b**2)
   
Si a tiene el valor 0 y b y c son mayores de 0, entonces utilizamos la fórmula: 
    a = √(c**2 - b**2)
    
Si b tiene el valor 0 y a y c son mayores de 0, entonces utilizamos la fórmula:
    b = √(c**2 - a**2) 

Donde a**2 significa a elevado al cuadrado, b**2 significa b 
elevado al cuadrado y c**2 significa c elevado al cuadrado; además,
a y b son catetos y c la hipotenusa del triángulo rectángulo.

El problema solo tiene sentido cuando los tres lados del triángulo tienen
longitudes positivas.
"""
import math

print("Escribe 0 en caso de no conocer la longitud\n")
a = float(input("Escribe la longitud del cateto a: "))
b = float(input("Escribe la longitud del cateto b: "))
c = float(input("Escribe la longitud de la hipotenusa c: "))

#a = math.sqrt(c**2 - b**2)
#b = math.sqrt(c**2 - a**2)
#c = math.sqrt(a**2 + b**2)

if c==0 and a>0 and b>0:     #and sirve para meter restricciones adicionales
    c = math.sqrt(a**2 + b**2)    
    mensaje = "El triángulo de cateto a = {} y cateto b = {}, " \
    "tiene hipotenusa c = {:0.4}".format(a, b, c)
      
elif a==0 and b>0 and c>0:     # Un and va después de una comparación
    a = math.sqrt(c**2 - b**2)   
    mensaje = "El triángulo de cateto b = {} e hipotenusa c = {}, " \
    "tiene cateto a = {:0.4}".format(b, c, a)

elif b==0 and a>0 and c>0:
    b = math.sqrt(c**2 - a**2)
    mensaje = "El triángulo de cateto a = {} e hipotenusa c = {}, " \
    "tiene cateto b = {:0.4}".format(a, c, b)
    
else:
    mensaje = "Solo uno de los valores puede ser cero "\
        "y los otros dos tienen que ser positivos"
print(mensaje)
    
# %% Catetos e hipotenusa de un triángulo rectángulo v5.0 17 03 2022

"""
Ejercicio individual

Problema
Dado un triángulo rectángulo de lados a, b, c unidades 
respectivamente, calcular el lado faltante.

Solución:

Identificar de los datos de entrada (a, b, c) cuál de ellos tiene el 
valor 0 y calcularlo haciendo uso de la fórmula correspondiente.

Si c tiene el valor 0 y a y b son mayores de 0, entonces utilizamos la fórmula:
   c = √(a**2 + b**2)
   
Si a tiene el valor 0 y b y c son mayores de 0, entonces utilizamos la fórmula: 
    a = √(c**2 - b**2)
    
Si b tiene el valor 0 y a y c son mayores de 0, entonces utilizamos la fórmula:
    b = √(c**2 - a**2) 

Donde a**2 significa a elevado al cuadrado, b**2 significa b 
elevado al cuadrado y c**2 significa c elevado al cuadrado; además,
a y b son catetos y c la hipotenusa del triángulo rectángulo.

El problema solo tiene sentido cuando los tres lados del triángulo tienen
longitudes positivas.
"""
import math

print("Escribe 0 en caso de no conocer la longitud\n")
a = float(input("Escribe la longitud del cateto a: "))
b = float(input("Escribe la longitud del cateto b: "))
c = float(input("Escribe la longitud de la hipotenusa c: "))

#a = math.sqrt(c**2 - b**2)
#b = math.sqrt(c**2 - a**2)
#c = math.sqrt(a**2 + b**2)

if c==0 and a>0 and b>0:     #and sirve para meter restricciones adicionales
    c = math.sqrt(a**2 + b**2)    
    mensaje = "El triángulo de cateto a = {} y cateto b = {}, " \
    "tiene hipotenusa c = {:0.4}".format(a, b, c)
      
elif a==0 and b>0 and c>0:     # No puede haber un elif sin un if
    a = math.sqrt(c**2 - b**2)   
    mensaje = "El triángulo de cateto b = {} e hipotenusa c = {}, " \
    "tiene cateto a = {:0.4}".format(b, c, a)

else:  #else no lleva condicionales y no puede haber un else sin un if
    b = math.sqrt(c**2 - a**2)
    mensaje = "El triángulo de cateto a = {} e hipotenusa c = {}, " \
    "tiene cateto b = {:0.4}".format(a, c, b)
    
print(mensaje)

# %% 
a = 15
if a ==0:
    print("Es cero")
elif a > 0:            #Puede haber tantos elif como sean necesarios
    print("Es positivo")
else:
    print("Es negativo")
    
    
# %% Clasificación de triángulos 22 03 2022
"""
Clasificación de triángulos
Dado un triángulo determina cuál es su clasificación de acuerdo a sus lados 
o a sus ángulos.

Solución

Clasificación por lados
Equilatero: todos sus lados iguales
Isóceles: 2 lados iguales y uno diferente
Escaleno: todos sus lados diferentes

Clasificación por ángulos (solo ángulos positivos)
Rectángulo: un ángulo recto
Acutángulo: todos sus ángulos son agudos <90°
Obtusángulo: tiene un ángulo mayor a 90° y menor a 180°

"""