# -*- coding: utf-8 -*-
"""
Created on Mon Feb 28 23:40:47 2022

@author: Ruíz Martínez Gustavo Rafael
"""
# %% Perímetro triángulo rectángulo 24 02 2022
"""
Problema
Dado un triángulo rectángulo de lados a, b, c cuyos valores son 3, 4, 5
unidades respectivamente, calcular:
    
    a)Perímetro
    
Solución
perímetro = a + b + c
"""    

a = 3
b = 4
c = 5

mensaje = "El triángulo tiene lados de tamaño 3, 4 y 5"
perímetro = a + b + c

print(mensaje)
print("El perímetro del triángulo es: {} unidades lineales.".
      format(perímetro))

# %% Área triángulo rectángulo 24 02 2022
"""
Problema
Dado un triángulo rectángulo de lados a, b, c cuyos valores son 3, 4, 5
unidades respectivamente, calcular:
    b)área
"""
mensaje2 = "El triángulo tiene una base de 4 (b) y una altura de 3 (a)."
mensaje3 = "El área se calcula multiplicando la base por la altura y dividiendo el resultado entre 2."
área = (a*b)/2
print(mensaje2)
print(mensaje3)
print("El área del triángulo es: {} unidades cuadradas.".format(área))

# %% Seno, coseno y tangente de un triángulo rectángulo 01 03 2022
"""
Problema
Dado un triángulo rectángulo de lados a, b, c cuyos valores son 3, 4, 5
unidades respectivamente, calcular:
    c)Seno, coseno y tangente
    d)Cotangente, secante y cosecante
    
Solución

seno(α) = a/c
coseno(α) = b/c
tangente(α) = a/b
cotangente(α) = b/a
secante(α) = c/b
cosecante(α) = c/a
"""
seno = a/c
coseno = b/c
tangente = a/b
cotangente = b/a
secante = c/b
cosecante = c/a

print("""Las razones trigonométricas del triángulo son:
      seno(α) = {}
      coseno(α) = {}
      tangente(α) = {}
      cotangente(α) = {:.3}   
      secante(α) = {}         
      cosecante(α) = {:.3}
""".format(seno, coseno, tangente,cotangente, secante, cosecante))

# {:4.3} el punto 3 quiere decir que contará dos decimales y el punto
# El 4 quiere decir que tendrá una tabulación de 4 espacios


# %% Seno, coseno y tangente de un triángulo rectángulo 03 03 2022
"""
Problema
Dado un triángulo rectángulo de lados a, b, c
 calcular:
    c)Seno, coseno y tangente
    d)Cotangente, secante y cosecante
    
Solución

seno(α) = a/c
coseno(α) = b/c
tangente(α) = a/b
cotangente(α) = b/a
secante(α) = c/b
cosecante(α) = c/a
"""
a = float(input("Escribe la longitud del lado a: "))
b = float(input("Escribe la longitud del lado b: "))
c = float(input("Escribe la longitud del lado c: "))

#float convierte la cadena (str) en flotante
#input se utiliza para que te salga un cuadro de texto y puedas ingresar 
#un valor. En las comillas se pone el texto. 

seno = a/c
coseno = b/c
tangente = a/b
cotangente = b/a
secante = c/b
cosecante = c/a

print("""Las razones trigonométricas del triángulo son:
      seno(α) = {}
      coseno(α) = {}
      tangente(α) = {}
      cotangente(α) = {:.3}   
      secante(α) = {}         
      cosecante(α) = {:.3}
""".format(seno, coseno, tangente,cotangente, secante, cosecante))

print("\a")  #Hace que suene un sonido en la compu.


# %% Cálculo de velocidad 03 03 2022

""" 
Problema
¿Cuál fue la velocidad de un móvil que se desplazó 15.4 metros en 4.3 
segundos?

Fórmula
v = d/t; donde v = velocidad [m/s], d = distancia [m] y t = tiempo [s]
"""

d = 15.4
t = 4.3

v = d/t

print("La velocidad del móvil que se desplazó 15.4 metros en 4.3 segundos fue de: ")

print("{:.3} m/s.".format(v))

# %% Calcular velocidad sin valores fijos 03 03 2022
"""
Calcular la velocidad de un móvil dependiendo de los valores de 
distancia y tiempo dados.

Fórmula
v = d/t; donde v = velocidad [m/s], d = distancia [m] y t = tiempo [s]

"""

d = float(input("Escribe la distancia (en metros) recorrida por el móvil: "))

t = float(input("Escribe el tiempo (en segundos) que el móvil tardó en hacer el recorrido: "))

v = d/t

print("La velocidad del móvil que se desplazó {} metros en {} segundos fue de: ".format(d, t))
print("{:.3} m/s.".format(v))


#from math import sqrt ==> se utiliza para importar la función
#de raíz cuadrada. Se tiene que hacer al inicio o antes de que ocupe 
#dicha función para que no me mande error. ==> sqrt(valor del que 
#quiero saber la raíz)

# %% Cateto de un triángulo rectángulo 10 03 2022

# Para importar bibliotecas puedo utilizar dos maneras:
    # ==>from math import sqrt, donde math es la biblioteca y 
    # sqrt es la función que quiero utilizar. Para utilizar la función,
    # basta con poner sqrt(valor)
    # ==>import math (con esto importo todo el módulo)
    # luego, usar c = math.sqrt(valor al que le ecesito aplicar la función)
    # el c = es para asignarle el valor a una variable
    # el math.sqrt da la instrucción de, de el módulo math que ya importamos
    # utiliza la función sqrt (raíz cuadrada)
    # Si solo escribo el nombre del módulo y un punto, por ejemplo,
    # math., pyhton te ayuda y te despliega las opciones de todas las
    # posibles funciones que ese módulo tiene. 
"""
Ejercicio individual

Problema
Dado un triángulo rectángulo de cateto a e hipotenusa c unidades 
respectivamente, calcular el cateto b.

Solución:

b = √(c**2 - a**2) ; donde b**2 significa a elevado al cuadrado

"""
import math

a = float(input("Escribe la longitud del cateto a: "))
c = float(input("Escribe la longitud de la hipotenusa c: "))

b = math.sqrt(c**2 - a**2)

print("El triángulo de cateto a = {} e hipotenusa c = {}, "
      "tiene cateto b = {:0.4}".format(a, c, b))