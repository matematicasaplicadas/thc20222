# -*- coding: utf-8 -*-
"""
Created on Tue Mar 29 12:19:10 2022

@author: Ruíz Martínez Gustavo Rafael
"""

# %% Clasificación de triángulos 22 03 2022
"""
Clasificación de triángulos
Dado un triángulo determina cuál es su clasificación de acuerdo a sus lados 
o a sus ángulos.

Solución

Clasificación por lados
Equilatero: todos sus lados iguales
Isóceles: 2 lados iguales y uno diferente
Escaleno: todos sus lados diferentes

Clasificación por ángulos (solo ángulos positivos)
Rectángulo: un ángulo recto
Acutángulo: todos sus ángulos son agudos <90°
Obtusángulo: tiene un ángulo mayor a 90° y menor a 180°

"""

# %% Cálculo de los ángulos de un triángulo 22 03 2022
"""
Problema
Dado el triángulo de lados a, b, c, calculor los ángulos A, B, C.

Solución
Para hacer el cálculo de los ángulos, necesitamos saber las siguientes 
razones trigonométricas:
    seno(α) = a/c
    coseno(α) = b/c
    tangente(α) = a/b
Donde a = al cateto opuesto, b = al cateto adyacente, c = a la hipotenusa
y α = al ángulo de referencia.
De estas fórmulas necesitamos despejar al ángulo α, por lo que tenemos que:
   α = arcseno(a/c)
   α = arccoseno(b/c)
   α = arctan(a/b) 
   
"""
import math
print("Escribe 0 si no conoces el valor de algún lado.")
a = float(input("Escribe la longitud del lado a: "))
b = float(input("Escribe la longitud del lado b: "))
c = float(input("Escribe la longitud del lado c: "))

if a == 0:
    A = math.acos(b/c)
    print("El ángulo A del triángulo de cateto b = {} e hipotenusa c = {} " \
          "tiene un valor de {}".format(b, c, A))
elif b == 0:
    A = math.asin(a/c)
    print("El ángulo A del triángulo de cateto a = {} e hipotenusa c = {} " \
         "tiene un valor de {}".format(a, c, A))
elif c == 0: 
    A = math.atan(a/b)
    print("El ángulo A del triángulo de cateto b = {} y cateto b = {} " \
          "tiene un valor de {}".format(a, b, A))
        
