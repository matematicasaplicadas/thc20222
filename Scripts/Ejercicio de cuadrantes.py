# -*- coding: utf-8 -*-
"""
Created on Tue Mar 22 11:00:58 2022

@author: Ruíz Martínez Gustavo Rafael
"""

# %% Cuadrantes del plano cartesiano 22 03 2022
"""
Problema
Dada una pareja ordenada, determinar en qué cuadrante del plano cartesiano
se encuentra.

Solución
El plano cartesiano se divine en cuatro cuadrantes. Para que podamos saber en
qué cuadrante se encuentra una pareja ordenada, nos guiaremos en lo siguiente:
    
    Cuadrante I: Los signos que deben de tener el número que está en la 
    primera posición y el número que está en la segunda posición de nuestra 
    pareja ordenada (normalmente "x" y "y") deben de ser ambos positivos.
    Cuadrante II: El signo del número de la primera posición de nuestra pareja 
    ordenada debe de ser negativo, mientras que el signo del número de la 
    segunda posición debe de ser positivo.
    Cuadrante III: El signo de los números de ambas posiciones de nuestra 
    pareja ordenada deben de ser negativos.
    Cuadrante IV: El signo del número de la primera posición de nuestra pareja 
    ordenada debe de ser positivo, mientras que el signo del número de la 
    segunda posición de nuestra pareja ordenada debe de ser negativo.
    
NOTA: Denotamos a una pareja ordenada compuesta por los números "x" y "y"
de la siguiente manera: (x,y)
Donde el número x, se encuentra en la primera posición de nuestra pareja 
ordenada y y se encuentra en la segunda posición de nuestra pareja ordenada.
NOTA 2: Si x = 0, y y es diferente de 0, la pareja ordenada se encuentra sobre
el eje y, por el contrario, si y = 0 y x es diferente de 0, la pareja ordenada
se encuentra sobre el eje x.
NOTA 3: Si x = 0 y y = 0, entonces la pareja ordenada se encuentra sobre el
origen.

"""

print("Escribe 1 si el signo del número es positivo y escribe 2 si" \
      " el signo del número es negativo. Escribe 0 si el número es 0.")
x = float(input("Escribe el signo del número que está en la primera posición " \
          "de la pareja ordenada: ") )
y = float(input("Escribe el signo del número que está en la segunda posición" \
          "de la pareja ordenada: "))

if x == 1 and y == 1:
    mensaje = "La pareja ordenada, con signos positivos en los números que se"\
        " encuentran en ambas posiciones, se encuentra en el cuadrante I."
    
elif x == 2 and y == 1:
    mensaje = "La pareja ordenada, con signo negativo en el número que se " \
        "encuentra en la primera posición y con signo positivo en el número" \
            " que se encuentra en la segunda posición, se encuentra en el" \
                "  cuadrante II."
    
elif x == 2 and y == 2:
    mensaje = "La pareja ordenada, con signos negativos en los números que se"\
        " encuentran en ambas posiciones, se encuentra en el cuadrante III."

elif x == 1 and y == 2:
    mensaje = "La pareja ordenada, con signo positivo en el número que se " \
        "encuentra en la primera posición y con signo negativo en el número" \
            " que se encuentra en la segunda posición, se encuentra en el" \
                "  cuadrante IV."
    
elif x == 0 and y != 0:
    mensaje = "La pareja ordenada se encuentra sobre el eje y."

elif y == 0 and x != 0:
    mensaje = "La pareja ordenada se encuentra sobre el eje x."
    
elif x == 0 and y == 0:
    mensaje = "La pareja ordenada se encuentra sobre el origen."

elif x>2 or x<0 or y>2 or y<0:
    mensaje = "Por favor, ingrese valores válidos."
    
print(mensaje)